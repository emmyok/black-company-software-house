const ranking = new Ranking('#numbers-ranking');
ranking.init();

const randomNumbersModel = new RandomNumbersModel();
const randomNumbersView = new RandomNumbersView('#latest-numbers');
const randomNumbersController = new RandomNumbersController();
randomNumbersController.init();