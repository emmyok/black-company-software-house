function Ranking(selector) {
    Component.call(this, selector);
    this.container = this.getDOMElement();
    this.numbers = [];
}

Ranking.prototype = Object.create(Component.prototype);
Ranking.constructor = Ranking;

Ranking.prototype.init = function () {
    const self = this;

    axios.get('http://localhost:3000/numbers')
        .then(function (response) {
            self.numbers = response.data.data.map(function (number) {
                randomNumbersModel.storedNumbers.set(number, 0);
                return {
                    id: number
                }
            });

            self.render();
        })
        .catch(function (error) {
            console.error(error);
        });
};

Ranking.prototype.createLi = function (number) {
    const listElement = document.createElement('li');
    listElement.classList.add('list-group-item');
    listElement.innerHTML = number;
    this.container.appendChild(listElement);
};

Ranking.prototype.render = function () {
    this.numbers.forEach(function (number) {
        this.createLi(number.id);
    }.bind(this));
};

Ranking.prototype.update = function () {
    this.clearPopularList();
    for (let [key] of randomNumbersModel.sortedNumbers) {
        this.createLi(key);
    }
};

Ranking.prototype.clearPopularList = function () {
    this.container.innerHTML = '';
};