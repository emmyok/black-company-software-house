function RandomNumbersView(selector) {
    Component.call(this, selector);
    this.container = this.getDOMElement();
}

RandomNumbersView.prototype = Object.create(Component.prototype);
RandomNumbersView.constructor = RandomNumbersView;

RandomNumbersView.prototype.render = function() {
    this.cleanContainer();
    randomNumbersModel.numbers.forEach(function(number) {
        this.createLi(number.id);
    }.bind(this));
};

RandomNumbersView.prototype.cleanContainer = function() {
    this.container.innerHTML = '';
};

RandomNumbersView.prototype.createLi = function (number) {
    const listElement = document.createElement('li');
    listElement.classList.add('list-group-item');
    listElement.innerHTML = number;
    this.container.appendChild(listElement);
};