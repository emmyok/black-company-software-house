function RandomNumbersModel() {
    this.numbers = [];
    this.storedNumbers = new Map();
    this.sortedNumbers = new Map();
}

RandomNumbersModel.constructor = RandomNumbersModel;

RandomNumbersModel.prototype.fetch = function(callback) {
    axios.get('http://localhost:3000/random-numbers')
        .then(function(response) {
            this.saveNumbers(response.data.data);
            callback();
        }.bind(this))
        .catch(function(error) {
            console.error(error);
        });
};

RandomNumbersModel.prototype.saveNumbers = function(numbers) {
    this.numbers = numbers.map(function(number) {
        let numberRepeatedTimes = randomNumbersModel.storedNumbers.get(number) + 1;
        randomNumbersModel.storedNumbers.set(number, numberRepeatedTimes);
        return {
            id: number
        }
    });
};