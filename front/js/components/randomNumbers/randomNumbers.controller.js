function RandomNumbersController() {
    this.refreshInterval = 10000;
}

RandomNumbersController.prototype = Object.create(Component.prototype);
RandomNumbersController.constructor = RandomNumbersController;

RandomNumbersController.prototype.init = function() {
    randomNumbersModel.fetch(this.randomNumbersFetched.bind(this));
    setInterval(function() {
        randomNumbersModel.fetch(this.randomNumbersFetched.bind(this));
    }.bind(this), this.refreshInterval);
};

RandomNumbersController.prototype.randomNumbersFetched = function() {
    randomNumbersView.render();
    randomNumbersModel.sortedNumbers = this.sortPopular();
    ranking.update();
};

RandomNumbersController.prototype.sortPopular = function () {
    return new Map([...randomNumbersModel.storedNumbers.entries()].sort(function(a, b) {
        return a[1] < b[1];
    }));
};